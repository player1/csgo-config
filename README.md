# CS config

> I am 𝔅𝔞𝔱𝔢𝔪𝔞𝔫𝔢 🦇

## start options

// launch options linux
vblank_mode=0 __GL_SYNC_TO_VBLANK=0 %command% -threads 4 +fps_max 120 -nosplash -novid +cl_interp 0 cl_interp_ratio 1 -high -nojoy

// win
-threads 4 +fps_max 0 -nosplash -novid +cl_interp 0 cl_interp_ratio 1 -high -nojoy -nod3d9ex


## todo

* [x] graffiti
* [x] nade
* [x] sound
* [x] win binds (z, w, a, q)
* [ ] line breaks compat win
* [ ] export in oneline (for c/p)
* [ ] video settings ?


